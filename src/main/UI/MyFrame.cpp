#include "MyFrame.hpp"
#include <wx/artprov.h>

// MyFrame类的事件表
BEGIN_EVENT_TABLE(MyFrame, wxFrame)
EVT_MENU(wxID_ABOUT, MyFrame::OnAbout)
EVT_MENU(wxID_EXIT, MyFrame::OnQuit)
EVT_SIZE(MyFrame::OnSize)
EVT_BUTTON(wxID_OK, MyFrame::OnButtonOk)
END_EVENT_TABLE()

#include "../../res/mondrian.xpm"
MyFrame::MyFrame(const wxString &title)
    : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(800, 600), wxNO_BORDER | wxFRAME_SHAPED)
{
    // 设置窗口的初始位置居中
    Center();
    // 设置窗口图标
    SetIcon(wxIcon(testLogo_xpm));

    // 创建菜单条
    wxMenu *fileMenu = new wxMenu;

    // 添加“关于”菜单项
    wxMenu *helpMenu = new wxMenu;
    helpMenu->Append(wxID_ABOUT, wxT("&About...\tF1"),
                     wxT("Show about dialog"));

    fileMenu->Append(wxID_EXIT, wxT("E&xit\tAlt-X"),
                     wxT("Quit this program"));

    // 将菜单项添加到菜单条中
    wxMenuBar *menuBar = new wxMenuBar();
    menuBar->Append(fileMenu, wxT("&File"));
    menuBar->Append(helpMenu, wxT("&Help"));

    // ...然后将菜单条放置在主窗口上
    SetMenuBar(menuBar);

    // 创建工具栏
    m_toolBar = new wxAuiToolBar(this, wxID_ANY);
    m_toolBar->AddTool(wxID_ANY, wxT("Minimize"), wxArtProvider::GetBitmap(wxART_MINUS));
    m_toolBar->AddTool(wxID_ANY, wxT("Maximize"), wxArtProvider::GetBitmap(wxART_PLUS));
    m_toolBar->AddTool(wxID_EXIT, wxT("Exit"), wxArtProvider::GetBitmap(wxART_QUIT));
    m_toolBar->Realize();

    m_mgr.SetManagedWindow(this);
    m_mgr.AddPane(m_toolBar, wxAuiPaneInfo().ToolbarPane().Top().Right());
    m_mgr.Update();

    wxPanel *panel = new wxPanel(this);
    wxButton *button = new wxButton(panel, wxID_OK, wxT("OK"),
                                    wxPoint(20, 20));

    m_mgr.AddPane(panel, wxAuiPaneInfo().CenterPane());
    m_mgr.Update();

    // Bind the exit menu item to the OnExit function
    Bind(wxEVT_MENU, &MyFrame::OnQuit, this, wxID_EXIT);

    button->SetFocus();

    // 创建一个状态条来让一切更有趣些。
    statusBas = CreateStatusBar(2);
    SetStatusText(wxT("Welcome to wxWidgets!"));
}

void MyFrame::OnAbout(wxCommandEvent &event)
{
    wxString msg;
    msg.Printf(wxT("Hello and welcome to %s"),
               wxVERSION_STRING);

    wxMessageBox(msg, wxT("About Minimal"),
                 wxOK | wxICON_INFORMATION, this);
}

void MyFrame::OnQuit(wxCommandEvent &event)
{
    // 释放主窗口
    Close();
}

void MyFrame::OnSize(wxSizeEvent &event)
{
    if (statusBas == nullptr)
    {
        return;
    }
    // 获取窗口的大小
    wxSize size = event.GetSize();
    wxString msg;
    msg.Printf(wxT("Size: %d x %d"), size.x, size.y);

    // 在状态条上显示窗口的大小
    SetStatusText(msg, 1);
}

void MyFrame::OnButtonOk(wxCommandEvent &event)
{
    wxMessageBox(wxT("OK button clicked!"));
}

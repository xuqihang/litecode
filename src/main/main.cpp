#include <iostream>
#include "main.hpp"
#include "UI/MyFrame.hpp"



// 有了这一行就可以使用 MyApp& wxGetApp()了
DECLARE_APP(MyApp)

// 告诉wxWidgets主应用程序是哪个类
IMPLEMENT_APP(MyApp)

// 初始化程序
bool MyApp::OnInit()
{
    // 创建主窗口
    MyFrame *frame = new MyFrame(wxT("Minimal wxWidgets App"));

    // 显示主窗口
    frame->Show(true);

    // 开始事件处理循环
    return true;
}

#ifndef MAIN_HPP
#define MAIN_HPP

#include <wx/wx.h>
// 定义应用程序类
class MyApp : public wxApp
{
public:
    // 这个函数将会在程序启动的时候被调用
    virtual bool OnInit();
};
#endif // MAIN_HPP
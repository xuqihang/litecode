#ifndef MYFRAME_HPP
#define MYFRAME_HPP

#include <wx/wx.h>
#include <wx/aui/aui.h>
// 定义主窗口类
class MyFrame : public wxFrame
{
public:
    // 主窗口类的构造函数
    MyFrame(const wxString &title);

    // 事件处理函数
    void OnQuit(wxCommandEvent &event);
    void OnAbout(wxCommandEvent &event);
    void OnSize(wxSizeEvent &event);
    void OnButtonOk(wxCommandEvent &event);

private:
    // 声明事件表
    DECLARE_EVENT_TABLE()
    wxStatusBar* statusBas = nullptr;
    
    wxAuiManager m_mgr;
    wxAuiToolBar* m_toolBar;
};
#endif // MYFRAME_HPP